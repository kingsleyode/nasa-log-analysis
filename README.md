# web-log-analysis!
This little project tries to analyse logs of a website to find some informations like user behavior or to see our page access rate in real time.


## user-behavior-analysis
This module analyses logs to understand users by creating a graph of theirs behaves in the website with Spark.
Example:

```mermaid
graph LR
A((s)) -- 80 --> B(Index)
B --20--> C
A --20--> C(page1)
B --60--> E(page2)
E --50--> F(page3)
E --10--> D
F --50--> D((t))
C --40--> D
```
- **s** represent the beginning of the connection
- **t** represent the end of the connection (we assume that if a user doesn't trigger any event for 1 hour, the connection is ended)


## realtime-dashboard

This module will analyse in realtime logs to get the top 10 page viewed in a 10 minute sliding window with Kafka and Spark.