package log.analysis

import java.time.LocalDateTime

import log.analysis.user.behavior.BehaviorGraph._
import log.analysis.Utils.Access
import org.apache.spark.graphx.{Edge, VertexId}
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}
import org.scalatest.{BeforeAndAfter, FunSuite}

class UtilsTest extends FunSuite with BeforeAndAfter{

  var sc: SparkContext = _
  val utils = new Utils

  before {

    // For windows
    System.setProperty("hadoop.home.dir","C:\\winutil\\")
    val conf = new SparkConf().setAppName("behavior-analysis-test").setMaster("local[*]")
    sc = new SparkContext(conf)

  }

  after{
    sc.stop
  }


  /**
    * TEST FOR VERTEX EXTRACT
    */
  test("testExtractVertex") {
    val access =
      Access("81.249.221.143",
        LocalDateTime.of(2009, 4, 6,14,39,27), "http://www.facades.fr/",
        200, 10743, "http://www.google.fr/search", "Mozilla/4.0")
    val accessPopUp =
      Access("81.249.221.143",
        LocalDateTime.of(2009, 4, 6,14,39,27), "http://www.facades.fr/popup.php?choix=1",
        200, 10743, "http://www.facades.fr/popup.php?choix=2", "Mozilla/4.0"
      )
    val accessPopUp2 =
      Access("81.249.221.143",
        LocalDateTime.of(2009, 4, 6,14,39,27), "http://www.facades.fr/popup.php?choix=13",
        200, 10743, "http://www.facades.fr/popup.php?choix=2", "Mozilla/4.0"
      )
    val rdd = sc.parallelize(List(access,accessPopUp, accessPopUp2))
    val result = utils.extractVertex(rdd).collect().map(_._2)

    val expected = Set(new S, new T, Resource("http://www.facades.fr/"), Resource("http://www.facades.fr/popup.php") )
    assert(result.length == expected.size)
    expected.foreach(vertex => result.contains(vertex))
  }

  /**
    * TEST FOR ASSETS
    */
  test("testIsAssets") {
    val access =
      Access("81.249.221.143",
        LocalDateTime.of(2009, 4, 6,14,39,27), "http://www.facades.fr/images/stands/TH_photoEtage_1.jpg",
        200, 10743, "http://www.facades.fr/exemples.php", "Mozilla/4.0"
      )
    val rdd = utils.filterAssets(sc.parallelize(List(access)))
    assert(rdd.isEmpty())

  }

  test("testIsNotAssets") {
    val access =
      Access("81.249.221.143",
        LocalDateTime.of(2009, 4, 6,14,39,27), "http://www.facades.fr/",
        200, 10743, "http://www.google.fr/search", "Mozilla/4.0"
      )
    val rdd = utils.filterAssets(sc.parallelize(List(access)))
    assert(! rdd.isEmpty())
  }

  test("testIsNotAssets2") {
    val access =
      Access("81.249.221.143",
        LocalDateTime.of(2009, 4, 6,14,39,27), "http://www.facades.fr/popup.php?choix=1",
        200, 10743, "http://www.facades.fr/popup.php?choix=2", "Mozilla/4.0"
      )
    val rdd = utils.filterAssets(sc.parallelize(List(access)))
    assert(! rdd.isEmpty())
  }


  /**
    * TEST FOR FLOOD EXTRACT
    */
  test("testExtractFlood") {
    val access =
      Access("81.249.221.143",
        LocalDateTime.of(2009, 4, 6,14,39,27), "http://www.facades.fr/",
        200, 10743, "http://www.google.fr/search", "Mozilla/4.0"
      )
    val accessPopUp =
      Access("81.249.221.143",
        LocalDateTime.of(2009, 4, 6,14,39,27), "http://www.facades.fr/popup.php?choix=1",
        200, 10743, "http://www.facades.fr/", "Mozilla/4.0"
      )
    val accessExemple =
      Access("81.249.221.145",
        LocalDateTime.of(2009, 4, 6,14,39,27), "http://www.facades.fr/exemple.php",
        200, 10743, "http://www.google.fr/search", "Mozilla/4.0"
      )
    val accessExemple2 =
      Access("86.250.221.122",
        LocalDateTime.of(2009, 4, 6,14,39,27), "http://www.facades.fr/exemple.php",
        200, 10743, "", "Mozilla/4.0")

    val vertexRdd: RDD[(VertexId, Vertex)] = sc.parallelize(
      List( (1L,Resource("http://www.facades.fr/")), (2L,Resource("http://www.facades.fr/popup.php")),
            (3L, Resource("http://www.facades.fr/exemple.php")), (4L, S()), (5L, T())))

    val accessRDD = sc.parallelize(List(access, accessPopUp, accessExemple, accessExemple2))

    val floods = utils.extractFlood(accessRDD, vertexRdd).collect()

    val result = Set(Edge(4L, 1L, Flood(1)), Edge(1L, 2L, Flood(1)), Edge(4L, 3L, Flood(2)))
    assert(floods.length == result.size)
    floods.foreach(edge => assert(result.contains(edge)))
    
  }

  /**
    *  TEST FOR LINESTOACCESS
    */
  test("testLinesToAccess") {
    val line = "81.249.221.143 - - [06/Apr/2009:14:39:27 +0200] \"GET /specialiste.php HTTP/1.1\" 200 10743 \"http://www.facades.fr/exemples.php\" \"Mozilla/4.0\" \"-\""
    val expected =
      Access("81.249.221.143",
        LocalDateTime.of(2009, 4, 6,14,39,27), "http://www.facades.fr/specialiste.php",
        200, 10743, "http://www.facades.fr/exemples.php", "Mozilla/4.0"
      )

    val rdd = sc.parallelize(List(line))
    assert(expected == utils.linesToAccess(rdd).take(1)(0))
  }

  test("testLinesToAccessForImage") {
    val line = "81.249.221.143 - - [06/Apr/2009:14:39:27 +0200] \"GET /images/stands/TH_photoEtage_1.jpg HTTP/1.1\" 200 3708 \"http://www.facades.fr/specialiste.php\" \"Mozilla/4.0\" \"-\""
    val expected =
      Access("81.249.221.143",
        LocalDateTime.of(2009, 4, 6,14,39,27), "http://www.facades.fr/images/stands/TH_photoEtage_1.jpg",
        200, 3708, "http://www.facades.fr/specialiste.php", "Mozilla/4.0"
      )
    val rdd = sc.parallelize(List(line))
    assert(expected == utils.linesToAccess(rdd).take(1)(0))
  }

  test("testLinesToAccessWithMissingProperties"){

    val line = "158.193.144.51 - - [06/Apr/2009:11:20:37 +0200] \"GET  HTTP/1.1\" 400 - \"-\" \"-\" \"-\""
    val expected =
      Access("158.193.144.51",
        LocalDateTime.of(2009, 4, 6,11,20,37), "http://www.facades.fr/",
        400, 0, "-", "-"
      )
    val rdd = sc.parallelize(List(line))
    assert(expected == utils.linesToAccess(rdd).take(1)(0))
  }

}
