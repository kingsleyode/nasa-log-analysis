package log.analysis

import java.time.{LocalDateTime, ZonedDateTime}
import java.util.Locale

import log.analysis.user.behavior.BehaviorGraph.{Resource, _}
import log.analysis.Utils.Access
import org.apache.spark.graphx.{Edge, VertexId}
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.dstream.DStream

class Utils {

  /**
    * Map a rdd of lines to an rdd of Access
    * @param linesRdd - a string
    * @return the access
    */
  def linesToAccess(linesRdd:RDD[String]): RDD[Access] = {
    val logPattern = "(.*) - - \\[(.*)\\] \"(.*)\" (\\d+) (.*) \"(.+)\" \"(.+)\" \"(.+)\"".r
    linesRdd.map { line =>
      logPattern.findFirstMatchIn(line) match {
        case Some(matcher) => Access(matcher.group(1), Utils.stringToDate(matcher.group(2)), Utils.getContent(matcher.group(3)), matcher.group(4).toInt, Utils.getResponseSize(matcher.group(5)), matcher.group(6), matcher.group(7))
        case _ => throw new IllegalArgumentException(s"The line : $line do not match the pattern $logPattern")
      }
    }
  }

  /**
    * Map a DStream of lines to an DStream of Access
    * @param linesStream - a string
    * @return the access
    */
  def linesToAccess(linesStream:DStream[String]): DStream[Access] = {
    val logPattern = "(.*) - - \\[(.*)\\] \"(.*)\" (\\d+) (.*) \"(.+)\" \"(.+)\" \"(.+)\"".r
    linesStream .map { line =>
      logPattern.findFirstMatchIn(line) match {
        case Some(matcher) => Access(matcher.group(1), Utils.stringToDate(matcher.group(2)), Utils.getContent(matcher.group(3)), matcher.group(4).toInt, Utils.getResponseSize(matcher.group(5)), matcher.group(6), matcher.group(7))
        case _ => throw new IllegalArgumentException(s"The line : $line do not match the pattern $logPattern")
      }
    }
  }



  /**
    * Retrieve only the access that are not for assets
    * @param accessRdd - the access rdd
    * @return the rdd filtered
    */
  def filterAssets(accessRdd: RDD[Access]): RDD[Access] = {
      accessRdd.filter { access =>
        ! ASSETS_EXTENSION.exists(access.content.endsWith)
      }
  }

  /**
    * Extract Vertex from the dataset
    * @param rdd - the rdd
    * @return the Vertex rdd
    */
  def extractVertex(rdd: RDD[Access]): RDD[(VertexId,Vertex)] = {
    rdd.map{ _.content }
      // Delete the url parameters
      .map{ _.replaceAll("\\?.*", "") }
      .distinct
      .map(Resource)
      .map(_.asInstanceOf[Vertex])
      // Add the source and the well
      .union(rdd.sparkContext.parallelize(List(S(), T())))
      // Create a unique index
      .zipWithIndex()
      .map{ case (vertex, vertexId) => (vertexId, vertex) }


  }


  /**
    *
    * Extract Edges from a rdd
    * @param rdd - the rdd containing access
    * @param vertexRdd - the rdd containing vertex
    * @return the edges rdd
    */
  def extractFlood(rdd: RDD[Access], vertexRdd: RDD[(VertexId,Vertex)] ): RDD[Edge[Flood]] = {
    val joinVertexRdd = vertexRdd.map { case (vertexId, vertex) => (vertex, vertexId) }
    joinVertexRdd.persist // Because it will be use multiple times

    rdd.map{ access =>
      // Delete the url parameters
      (access.content.replaceAll("\\?.*", "") , access.origin.replaceAll("\\?.*", "")) }

      .map{ case (content, origin) => (Resource(content).asInstanceOf[Vertex], origin) }
      // Join with the content url to have the vertex Id
      .join(joinVertexRdd)
      .map{ case (_, (origin, vertexContentId)) => if(origin.startsWith(ROOT_URL)) (Resource(origin).asInstanceOf[Vertex], vertexContentId) else (S().asInstanceOf[Vertex], vertexContentId) }
      // Join with origin url to have the vertex Id
      .join(joinVertexRdd)
      .map{ case(_, (vertexContentId, vertexOriginId)) => ((vertexOriginId, vertexContentId), 1) }
      .reduceByKey(_+_)
      .map{ case((vertexOriginId, vertexContentId),number) => Edge(vertexOriginId, vertexContentId, Flood(number)) }

  }

}

object Utils {

  /**
    * This class represent an access in the log
    * @param ip - the ip of the user
    * @param date - the date pf the user
    * @param content - the content of the request
    * @param responseCode - the response code of the request
    * @param responseBodySize - the size of the response
    * @param origin - the origin of the request.
    * @param clientVersion - the client version
    */
  case class Access(ip: String, date: LocalDateTime, content: String,
                    responseCode: Integer, responseBodySize: Long,
                    origin: String, clientVersion: String)

  /**
    * Convert the string to a date
    * @param string -  the string to convert
    * @return - the date
    */
  private [Utils] def stringToDate(string: String): LocalDateTime = {
    import java.time.format.DateTimeFormatter
    val formatter = DateTimeFormatter.ofPattern("d/MMM/yyyy:HH:mm:ss Z").withLocale(Locale.ENGLISH)
    // The LocalDate do not support timezone information, so we have to use ZonedDateTime first
    ZonedDateTime.parse(string, formatter).toLocalDateTime
  }

  /**
    * Get the content of the access
    * @param string - the string of the log
    * @return the content of the access
    */
  private [Utils] def getContent(string: String): String = {
    val route = string.split(" ")(1)
    ROOT_URL + route.stripPrefix("/").stripSuffix("/")
  }

  /**
    * Get the response size
    * @param string - the string in the log
    * @return the response size
    */
  private [Utils] def getResponseSize(string: String): Long = {
    string match {
      case "-" => 0
      case other => other.toInt
    }
  }
}
