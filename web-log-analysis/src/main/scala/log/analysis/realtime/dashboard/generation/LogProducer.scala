package log.analysis.realtime.dashboard.generation

import akka.actor.ActorSystem
import log.analysis.realtime.dashboard.KAFKA_SERVER
import User.Go

import scala.concurrent.duration._

object LogProducer extends App {

  val system: ActorSystem = ActorSystem("log-producer")
  import system.dispatcher


    val kafkaLogProducer = system.actorOf(KafkaLogProducer.props(KAFKA_SERVER  ,system.settings.config.getConfig("akka.kafka.producer")
    ))
    val user = system.actorOf(User.props("88.191.254.20", kafkaLogProducer ))

    system.scheduler.schedule( 0 millisecond ,50 milliseconds, user, Go)


}
