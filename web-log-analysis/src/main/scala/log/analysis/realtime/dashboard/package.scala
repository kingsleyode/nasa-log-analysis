package log.analysis.realtime

package object dashboard {
  val LOG_TOPIC = "log"
  val KAFKA_SERVER= "localhost:9092"

  val pages = Set("index.php", "qui-sommes-nous.php", "nos-offres.php", "nos-references.php",
    "nos-produits.php", "contact.php", "produit.php?id=48")


}
