package log.analysis.realtime.dashboard.generation

import akka.actor.{Actor, ActorLogging, Props}
import akka.kafka.ProducerSettings
import akka.kafka.scaladsl.Producer
import akka.stream.scaladsl.Source
import akka.stream.{ActorMaterializer, ActorMaterializerSettings}
import com.typesafe.config.Config
import log.analysis.realtime.dashboard.generation.KafkaLogProducer.Log
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.StringSerializer

class KafkaLogProducer(producerSettings: ProducerSettings[String,String]) extends Actor with ActorLogging{
  final implicit val materializer: ActorMaterializer = ActorMaterializer(ActorMaterializerSettings(context.system))
  override def receive : Receive = {
    case Log(line) => Source(List(line)).map(value => new ProducerRecord[String, String]("log", value))
      .runWith(Producer.plainSink(producerSettings))
  }

}


object KafkaLogProducer {

  case class Log(line: String)


  def props(bootstrapServer: String, config: Config): Props = {
    val producerSettings =
      ProducerSettings(config, new StringSerializer, new StringSerializer)
        .withBootstrapServers(bootstrapServer)
    Props(new KafkaLogProducer(producerSettings))
  }


}
