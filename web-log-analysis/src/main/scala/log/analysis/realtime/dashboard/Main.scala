package log.analysis.realtime.dashboard
import log.analysis.Utils
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.spark.streaming.{Minutes, Seconds, StreamingContext}
import org.apache.spark.SparkConf
import org.apache.spark.streaming.kafka010.KafkaUtils
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe

object Main extends App {
  System.setProperty("hadoop.home.dir","C:\\winutil\\")

  val sparkConf = new SparkConf().setAppName("RealtimeDashBoard").setMaster("local[2]")
  val ssc = new StreamingContext(sparkConf, Seconds(1))

  val kafkaParams = Map[String, Object](
    "bootstrap.servers" -> "localhost:9092",
    "key.deserializer" -> classOf[StringDeserializer],
    "value.deserializer" -> classOf[StringDeserializer],
    "group.id" -> "use_a_separate_group_id_for_each_stream",
    "auto.offset.reset" -> "latest",
    "enable.auto.commit" -> (false: java.lang.Boolean)
  )

  val topics = Array("log")
  val stream = KafkaUtils.createDirectStream(
    ssc,
    PreferConsistent,
    Subscribe[String, String](topics, kafkaParams)
  )
  val utils = new Utils

  utils.linesToAccess(stream.map( _.value()))
    .map{ access => (access.content, 1) }
    .reduceByKeyAndWindow(_ + _, Minutes(10))
    .print(3)

  ssc.start()
  ssc.awaitTermination()



}
