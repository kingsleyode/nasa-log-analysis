package log.analysis.realtime.dashboard.generation

import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.util.Locale

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import log.analysis.realtime.dashboard.generation.KafkaLogProducer.Log
import log.analysis.realtime.dashboard.generation.User.Go
import log.analysis.realtime.dashboard.pages

import scala.util.Random

class User(ip: String, kafkaLogProducer: ActorRef) extends Actor with ActorLogging{
  override def receive: Receive = {
    case Go => kafkaLogProducer ! Log(generateLog())
  }

  def generateLog(): String = {
    val formatter = DateTimeFormatter.ofPattern("d/MMM/yyyy:HH:mm:ss Z").withLocale(Locale.ENGLISH)
    val now = ZonedDateTime.now().format(formatter)
    val rnd = new Random
    val page = pages.toVector(rnd.nextInt(pages.size))

    s"$ip - - [" + now + "] \"GET /" + page + "HTTP/1.0\" 200 8674 \"-\" \"-\" \"-\""
  }
}

object User {
  def props(ip: String, kafkaLogProducer: ActorRef): Props = Props(new User(ip, kafkaLogProducer))
  case class Go()
}
