package log.analysis.user.behavior

import log.analysis.Utils
import org.apache.spark.graphx.Graph
import org.apache.spark.rdd.RDD

object BehaviorGraph{

  type BehaviorGraph = Graph [Vertex, Flood]

  /**
    * A vertx of a BehaviorGraph
    */
  sealed trait Vertex


  /**
    * The Source of the connection
    */
  case class S() extends Vertex

  /**
    * The end of the connection
    */
  case class T() extends Vertex

  /**
    * A resources, representing a vertex of a behavior graph
    * @param url - the url of the resource
    */
  case class Resource(url: String) extends Vertex

  /**
    * A flood, representing an edge of a behavior graph
    * @param number - the number of users taking
    */
  case class Flood(number: Long)

  /**
    * Create a BehaviorGraph from logs
    * @param logs - an RDD containing logs
    * @return the BehaviorGraph
    */
  def apply(logs: RDD[String]): BehaviorGraph = {
    val utils = new Utils
    val access = utils.filterAssets(utils.linesToAccess(logs))
      .filter(access => access.responseCode <= 400)
    access.persist()
    val vertex = utils.extractVertex(access)
    val edges = utils.extractFlood(access, vertex)
    Graph(vertex, edges)
  }
}

