package log.analysis.user.behavior

import org.apache.spark.{SparkConf, SparkContext}

object Main extends App {
  System.setProperty("hadoop.home.dir","C:\\winutil\\")
  val conf = new SparkConf().setAppName("behavior-analysis").setMaster("local[*]")
  val sc = new SparkContext(conf)

  val graph = BehaviorGraph(sc.textFile("access.log"))

  println(graph.pageRank(0.0001).vertices.collect.mkString("\n"))
}
