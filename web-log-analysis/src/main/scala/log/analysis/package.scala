package log

package object analysis {
  val ROOT_URL = "http://www.facades.fr/"
  val ASSETS_EXTENSION = Set("jpg", "gif", "png", "jpeg")
}
